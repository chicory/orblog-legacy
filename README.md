## orblog (0.1.1)
Simple blog for hidden services (like tor / i2p) with markdown support.

## Features
* Without JavaScript.
* Posts with markdown support.
* Comments with markdown support.
* Categories.
* View posts by tags.
* RSS feed

## System requirements
* PHP 7.3 +
* GDLib
* SQLite3

## Dev run
```
git clone https://codeberg.org/chicory/orblog-legacy.git
cd orblog
php -S 127.0.0.1:8000
```
**Admin panel:**
```
hostname/?view=admin
password: changeme
```

## Installation
* Edit config file (./config.php).
* Set permissions.
  * For ./private/data 775
  * For ./private/data/data.sqlite 664
  * For other files 644
* Deny access to ./private directory at web server.
* Open your_domain/?view=admin in web browser.

