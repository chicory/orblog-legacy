<?php
/*-----------------------------------------------------------------------------
    
  orblog - Simple blog for hidden networks.  
  
    Version:   0.1
    Git:       https://codeberg.org/chicory/orblog-legacy
    Copyright: chicory@disroot.org 2020
    License:   http://www.apache.org/licenses/LICENSE-2.0

-----------------------------------------------------------------------------*/

$_TPL['title'] = $_LANG['error_404'].' | '.TITLE;

include_template('header');
include_template('404');
