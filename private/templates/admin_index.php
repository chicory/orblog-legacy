<?php
/*-----------------------------------------------------------------------------
    
  orblog - Simple blog for hidden networks.  
  
    Version:   0.1
    Git:       https://codeberg.org/chicory/orblog-legacy
    Copyright: chicory@disroot.org 2020
    License:   http://www.apache.org/licenses/LICENSE-2.0

-----------------------------------------------------------------------------*/
?>
          <h1><?=$_LANG['stats']?></h1>
          <ul>
            <li><?=$_LANG['all_posts_count']?> : <?=$_TPL['posts']?></li>
            <li><?=$_LANG['all_comm_count']?> : <?=$_TPL['comments']?></li>                       
          </ul>

