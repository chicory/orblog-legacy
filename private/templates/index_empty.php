<?php
/*-----------------------------------------------------------------------------
    
  orblog - Simple blog for hidden networks.  
  
    Version:   0.1
    Git:       https://codeberg.org/chicory/orblog-legacy
    Copyright: chicory@disroot.org 2020
    License:   http://www.apache.org/licenses/LICENSE-2.0

-----------------------------------------------------------------------------*/
?>  
        <div class="clear"></div>
        <div class="content"><br>
          <div class="center">
            <h1><?=$_LANG['empty_category']?></h1>          
          </div>
